﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace AlienRunExample
{
    class Player
    {
        // ---------------------------
        // Data
        // ---------------------------
        Animation animation; 
        Vector2 position = Vector2.Zero;
        Vector2 previousPosition = Vector2.Zero;
        Vector2 velocity = Vector2.Zero;
        bool touchingGround = false;
        bool jumping = false;
        float jumpTime = 0f;
        Level level = null;
        SoundEffect jumpSound = null;
        int score = 0;
        Vector2 size;

        // Constants
        private const float MOVE_SPEED = 300.0f;
        private const float GRAVITY_ACCEL = 3400.0f;
        private const float TERMINAL_VEL = 550.0f;
        private const float JUMP_LAUNCH_VEL = -1500.0f;
        private const float JUMP_CONTROL_POWER = 0.05f;
        private const float MAX_JUMP_TIME = 0.3f;

        // ---------------------------
        // Behaviour
        // ---------------------------
        public Player(Level newLevel)
        {
            level = newLevel;
        }
        // ---------------------------
        public void SetPosition(Vector2 newPosition)
        {
            position = newPosition;
        }
        // ---------------------------
        public void LoadContent(ContentManager content)
        {
            Texture2D sprite = content.Load<Texture2D>("graphics/player/player-animations");
            size = new Vector2(72, 97);
            animation = new Animation(sprite, (int)size.X, (int)size.Y, 15);
            animation.AddClip("run", 0, 4);
            animation.AddClip("jump", 5, 5);

            // Play the "run" animation by default
            animation.PlayClip("run");
        }
        // ---------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            animation.Draw(spriteBatch, position);
        }
        // ---------------------------
        public void Update(GameTime gameTime)
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Update our velocity based on inputs, jumping, gravity, etc.
            velocity.X = MOVE_SPEED; // infinite runner so we just set this, 
                                     // otherwise we'd use acceleration here.
            velocity.Y += GRAVITY_ACCEL * frameTime; // Apply acceleration due to gravity
            velocity.Y = MathHelper.Clamp(velocity.Y, -TERMINAL_VEL, TERMINAL_VEL); // Clamp to within allowed vertical speed

            // Apply input (jumping)
            Input(gameTime);

            // Update position based on velocity and frame time
            previousPosition = position;
            position += velocity * frameTime;

            // Check tile collision at new location
            CheckTileCollision();

            // Update our animation
            if (touchingGround)
                animation.PlayClip("run");
            else
                animation.PlayClip("jump");
            animation.Update(gameTime);
        }
        // ---------------------------
        public Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);
        }
        // ---------------------------
        public Rectangle GetPreviousBounds()
        {
            return new Rectangle((int)previousPosition.X, (int)previousPosition.Y, (int)size.X, (int)size.Y);
        }
        // ---------------------------
        public void CheckTileCollision()
        {
            // Start off the check by assuming we are not touching the ground
            touchingGround = false;

            // Using the player's bounding box in absolute space, determine our bounding box in terms of tile coordinates
            Rectangle bounds = GetBounds();
            Rectangle previousBounds = GetPreviousBounds();
            List<Tile> collidingTiles = level.GetTilesInBounds(bounds);
            
            // For each potentially colliding tile,
            foreach (Tile collision in collidingTiles)
            {
                // Determine collision depth (with direction) and magnitude.
                Rectangle tileBounds = collision.GetBounds();
                Vector2 depth = collision.GetCollisionDepth(bounds);
                Tile.CollisionType type = collision.GetCollisionType();

                // If the depth is not zero, it means we are colliding with this tile
                if (depth != Vector2.Zero)
                {
                    // If the collision type was lethal, goal, or points, no need to check anything else
                    if (type == Tile.CollisionType.LETHAL)
                    {
                        level.SetupLevel();
                        velocity = Vector2.Zero;
                        jumping = false;
                        jumpTime = 0;
                        score = 0;
                        return; // exit function early, no need to do more as we're dead!
                    }
                    else if (type == Tile.CollisionType.GOAL)
                    {
                        level.CompleteLevel();
                        return; // exit function early, no need to do more as we've won!
                    }
                    else if (type == Tile.CollisionType.POINTS)
                    {
                        score += collision.GetPoints();
                        collision.Hide();
                        continue; // Do NOT exit completely, but skip to the next tile in the list of tiles we're colliding with
                    }

                    float absDepthX = Math.Abs(depth.X);
                    float absDepthY = Math.Abs(depth.Y);

                    // Resolve the collision along the shallow axis, as that is the 
                    // one we're closer to the edge on and therefore easier to "squeeze out"
                    // First we check the vertical axis
                    // If this is a platform tile, this is the ONLY axis we check!
                    if (absDepthY < absDepthX)
                    {

                        // Only actually move us if this is an impassable tile, not a platform tile
                        // OR if we have determined we're colliding from above (platforms should block if falling onto them)
                        if (type == Tile.CollisionType.IMPASSABLE || 
                            (type == Tile.CollisionType.PLATFORM && bounds.Bottom > tileBounds.Top && previousBounds.Bottom <= tileBounds.Top))
                        {
                            // If our feet were slightly below the top of this tile, set us to being on the ground.
                            // Remember that in this case a higher number means farther down (lower) on the screen!
                            if (bounds.Bottom >= tileBounds.Top)
                            {
                                touchingGround = true;
                            }

                            // Resolve the collision along the Y axis.
                            position = new Vector2(position.X, position.Y + depth.Y);

                            // Recalculate bounds for future collision checking
                            bounds = GetBounds();
                        }
                    }
                    else
                    {
                        // Only actually move us if this is an impassable tile, not a platform tile
                        // Platform tiles shouldn't block us from the sides.
                        if (type == Tile.CollisionType.IMPASSABLE)
                        {
                            // Resolve the collision along the X axis.
                            position = new Vector2(position.X + depth.X, position.Y);

                            // Recalculate bounds for future collision checking
                            bounds = GetBounds();
                        }
                    }
                }
            }
        }
        // ---------------------------
        public void Input(GameTime gameTime)
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            KeyboardState keystate = Keyboard.GetState();

            // Are we allowed to jump?
            // only true if we are touching the ground (starting a jump) 
            // or if we are holding down the button already and haven't reached our max jump time
            bool allowedToJump = touchingGround == true || (jumping == true && jumpTime <= MAX_JUMP_TIME);

            // simple version: allowedToJump = touchingGround;

            // Check if the player is jumping
            if (keystate.IsKeyDown(Keys.Space) && allowedToJump)
            {
                // Play a sound if we have just started our jump
                //if (jumping = false)
                //    jumpSound.Play();

                jumping = true;
                jumpTime += frameTime; // remove for simple version

                // Fully override the vertical velocity with a power curve that gives players more control over the top of the jump
                velocity.Y = JUMP_LAUNCH_VEL;// * (1.0f - (float)Math.Pow(jumpTime / MAX_JUMP_TIME, JUMP_CONTROL_POWER));

                // simple: velocity.Y = JUMP_LAUNCH_VEL;
            }
            else
            {
                jumping = false;
            }
        }
        // ---------------------------
        public Vector2 GetPosition()
        {
            return position;
        }
        // ---------------------------
        public int GetScore()
        {
            return score;
        }
        // ---------------------------
    }
}
