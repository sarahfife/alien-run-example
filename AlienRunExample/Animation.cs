﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.Diagnostics;

namespace AlienRunExample
{
    class Animation
    {
        // ---------------------------
        // Types
        // ---------------------------
        struct Clip
        {
            public int startFrame;
            public int endFrame;
            public bool looping;
        }

        // ---------------------------
        // Data
        // ---------------------------

        // Settings
        private Texture2D sprite = null;
        private int frameWidth = 32;
        private int frameHeight = 32;
        private float framesPerSecond = 30;
        private Dictionary<string, Clip> clips = new Dictionary<string, Clip>();

        // Runtime
        private string currentClip = "";
        private int currentFrame = 0;
        private float timeInFrame = 0;

        // ---------------------------
        // Behaviour
        // ---------------------------
        public Animation(Texture2D newSprite, int newFrameWidth, int newFrameHeight, float newFPS = 30)
        {
            sprite = newSprite;
            frameWidth = newFrameWidth;
            frameHeight = newFrameHeight;
            framesPerSecond = newFPS;
        }
        // ---------------------------
        public void AddClip(string name, int startFrame, int endFrame, bool isLooping = true)
        {
            Clip newClip = new Clip();
            newClip.startFrame = startFrame;
            newClip.endFrame = endFrame;
            newClip.looping = isLooping;
            clips.Add(name, newClip);
        }
        // ---------------------------
        public void PlayClip(string name, bool reset = false)
        {
            // Don't bother messing with things if the clip is the current one
            // Unless we've said we want to reset the clip
            if (name == currentClip && reset == false)
                return;


            // Make sure this clip is in the list
            // If not, stop execution and inform the tester
            bool clipSetup = clips.ContainsKey(name);
            Debug.Assert(clipSetup, "Clip "+name+" not found in animation");

            // That will only happen in debug builds, so is good for testing and won't affect release
            // But this means we need to check for release as well and only do stuff if there is a clip
            if (clipSetup)
            {
                currentClip = name;
                currentFrame = clips[name].startFrame;
                timeInFrame = 0;
            }

        }
        // ---------------------------
        public void Update (GameTime gameTime)
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Add to how long we've spent in this frame
            timeInFrame += frameTime;

            // Determine if it's time for a new frame
            float timePerFrame = 1.0f / framesPerSecond; // --> seconds per frame (inverse of frames per second)
            if (timeInFrame >= timePerFrame)
            {
                // It's time for a new frame
                Clip clip = clips[currentClip];
                ++currentFrame;
                timeInFrame = 0;
                // If this would cause us to pass our last frame...
                if (currentFrame > clip.endFrame)
                {
                    // If we're looping, return to the start of the clip
                    if (clip.looping == true)
                    {
                        currentFrame = clip.startFrame;
                    }
                    // If we are not looping, just stay on the last frame
                    else
                    {
                        currentFrame = clip.endFrame;
                    }
                }
            }
        }
        // ---------------------------
        public void Draw(SpriteBatch spriteBatch, Vector2 position)
        {
            int numFramesX = sprite.Width / frameWidth;
            int xFrameIndex = currentFrame % numFramesX;
            int yFrameIndex = currentFrame / numFramesX;

            Rectangle source = new Rectangle(xFrameIndex * frameWidth, yFrameIndex * frameHeight, frameWidth, frameHeight);

            spriteBatch.Draw(sprite, position, source, Color.White);

        }
        // ---------------------------
    }
}
