﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AlienRunExample
{
    class Tile
    {
        // ---------------------------
        // Enums
        // ---------------------------
        public enum CollisionType
        {
            PASSABLE,   // = 0, player can pass through freely
            IMPASSABLE, // = 1, blocks player movement
            PLATFORM,   // = 2, blocks movement downward only
            LETHAL,     // = 3, kills the player
            GOAL,       // = 4, player wins level
            POINTS,     // = 5, player gets points
        }


        // ---------------------------
        // Data
        // ---------------------------
        private Texture2D sprite;
        private Vector2 position;
        private CollisionType collision;
        private int points = 0;
        bool visible = true;


        // ---------------------------
        // Behaviour
        // ---------------------------
        public Tile(Texture2D newSprite, Vector2 newPosition, CollisionType newCollision, int newPoints = 0)
        {
            sprite = newSprite;
            position = newPosition;
            collision = newCollision;
            points = newPoints;
        }
        // -------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (visible)
                spriteBatch.Draw(sprite, position, Color.White);
        }
        // ---------------------------
        public CollisionType GetCollisionType()
        {
            return collision;
        }
        // ---------------------------
        public Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)position.X+ sprite.Width, (int)position.Y+ sprite.Height);
        }
        // ---------------------------
        public Vector2 GetCollisionDepth(Rectangle otherRect)
        {
            // The amount of overlap between two intersecting rectangles. These
            // depth values can be negative depending on which wides the rectangles
            // intersect. This allows callers to determine the correct direction
            // to push objects in order to resolve collisions.
            // If the rectangles are not intersecting, Vector2.Zero is returned.

            // Check will be between tile Bounds and colliding (other) rectangle
            Rectangle tileBounds = GetBounds();

            // Calculate half sizes.
            float halfWidthOther = otherRect.Width / 2.0f;
            float halfHeightOther = otherRect.Height / 2.0f;
            float halfWidthTile = tileBounds.Width / 2.0f;
            float halfHieghtTile = tileBounds.Height / 2.0f;

            // Calculate centers.
            Vector2 centerOther = new Vector2(otherRect.Left + halfWidthOther, otherRect.Top + halfHeightOther);
            Vector2 centerTile = new Vector2(tileBounds.Left + halfWidthTile, tileBounds.Top + halfHieghtTile);

            // Calculate current and minimum-non-intersecting distances between centers.

            // Distance between the centers of A and B, on the X and Y coordinate
            float distanceX = centerOther.X - centerTile.X;
            float distanceY = centerOther.Y - centerTile.Y;

            // Minimum distance these need to be to NOT intersect
            // AKA if they are AT LEAST this far away in either direction, they are not touching.
            float minDistanceX = halfWidthOther + halfWidthTile;
            float minDistanceY = halfHeightOther + halfHieghtTile;

            // If we are not intersecting at all, return (0, 0).
            // If either the X or Y distance is greater than their minimum, we are NOT intersecting
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= minDistanceY)
                return Vector2.Zero;

            // Calculate and return intersection depths.
            // Essentially, how much over the minimum intersection distance are we in each direction
            // AKA by how much are they intersection in that direction?
            float depthX, depthY;

            if (distanceX > 0)
                depthX = minDistanceX - distanceX;
            else
                depthX = -minDistanceX - distanceX;

            if (distanceY > 0)
                depthY = minDistanceY - distanceY;
            else
                depthY = -minDistanceY - distanceY;

            return new Vector2(depthX, depthY);
        }
        // ---------------------------
        public int GetPoints()
        {
            return points;
        }
        // ---------------------------
        public bool GetVisible()
        {
            return visible;
        }
        // ---------------------------
        public void Hide()
        {
            visible = false;
        }
        // ---------------------------
    }
}
