﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AlienRunExample
{
    class Level
    {
        // ---------------------------
        // Data
        // ---------------------------

        // map setup
        const int LEVEL_WIDTH = 100;
        const int LEVEL_HEIGHT = 100;

        const int TILE_WIDTH = 70;
        const int TILE_HEIGHT = 70;

        private Tile[,] tiles;
        private Player player;
        private Vector2 viewSize;

        Texture2D boxTexture;
        Texture2D bridgeTexture;
        Texture2D spikeTexture;
        Texture2D goalTexture;
        Texture2D bronzeCoinTexture;
        Texture2D silverCoinTexture;
        Texture2D goldCoinTexture;
        SpriteFont mainFont;
        SpriteFont largeFont;

        bool complete = false;


        // ---------------------------
        // Behaviour
        // ---------------------------
        public void SetViewSize(Vector2 newViewSize)
        {
            viewSize = newViewSize;
        }
        // ---------------------------
        public void LoadContent(ContentManager content)
        {
            // Create the player
            player = new Player(this);
            player.LoadContent(content);

            // Load in the tile textures
            boxTexture = content.Load<Texture2D>("graphics/tiles/box");
            bridgeTexture = content.Load<Texture2D>("graphics/tiles/bridge");
            spikeTexture = content.Load<Texture2D>("graphics/tiles/spikes");
            goalTexture = content.Load<Texture2D>("graphics/tiles/goal");
            bronzeCoinTexture = content.Load<Texture2D>("graphics/items/coinBronze");
            silverCoinTexture = content.Load<Texture2D>("graphics/items/coinSilver");
            goldCoinTexture = content.Load<Texture2D>("graphics/items/coinGold");

            // Load font to be used for winning message, score
            mainFont = content.Load<SpriteFont>("fonts/mainFont");
            largeFont = content.Load<SpriteFont>("fonts/largeFont");

            // Set up the level initially (also done on player death)
            SetupLevel();
        }
        // ---------------------------
        public void SetupLevel()
        {
            // Reset completion status
            complete = false;

            // Allocate the tile grid.
            tiles = new Tile[LEVEL_WIDTH, LEVEL_HEIGHT];

            // Set player starting position
            SetupPlayer(0, 0);

            // Place some solid blocks
            CreateBox(0, 4, boxTexture);
            CreateBox(1, 4, boxTexture);
            CreateBox(2, 4, boxTexture);
            CreateBox(3, 4, boxTexture);
            CreateBox(4, 4, boxTexture);
            CreateBox(5, 4, boxTexture);
            CreateBox(6, 4, boxTexture);
            CreateBox(6, 3, boxTexture);
            CreateBox(10, 5, boxTexture);
            CreateBox(11, 5, boxTexture);
            CreateBox(12, 5, boxTexture);
            CreateBox(13, 5, boxTexture);
            CreateBox(14, 5, boxTexture);

            // Place some pass-through platforms
            CreateBridge(3, 2, bridgeTexture);
            CreateBridge(4, 2, bridgeTexture);
            CreateBridge(6, 1, bridgeTexture);
            CreateBridge(7, 1, bridgeTexture);
            CreateBridge(8, 1, bridgeTexture);
            CreateBridge(9, 1, bridgeTexture);

            // Place some spikes
            CreateSpike(9, 6, spikeTexture);
            CreateSpike(7, 6, spikeTexture);
            CreateSpike(8, 6, spikeTexture);
            CreateSpike(9, 6, spikeTexture);

            // Place some coins
            CreateBronzeCoin(2, 3);
            CreateBronzeCoin(3, 3);
            CreateBronzeCoin(4, 3);
            CreateSilverCoin(3, 1);
            CreateSilverCoin(4, 1);
            CreateGoldCoin(8, 0);

            // Place the goal
            CreateGoal(14, 4, goalTexture);
        }
        // ---------------------------
        public void SetupPlayer(int tileX, int tileY)
        {
            player.SetPosition(new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT));
        }
        // ---------------------------
        public void CreateBox(int tileX, int tileY, Texture2D tileTexture)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(tileTexture, tilePosition, Tile.CollisionType.IMPASSABLE);
            tiles[tileX, tileY] = newTile;
        }
        // ---------------------------
        public void CreateBridge(int tileX, int tileY, Texture2D tileTexture)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(tileTexture, tilePosition, Tile.CollisionType.PLATFORM);
            tiles[tileX, tileY] = newTile;
        }
        // ---------------------------
        public void CreateSpike(int tileX, int tileY, Texture2D tileTexture)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(tileTexture, tilePosition, Tile.CollisionType.LETHAL);
            tiles[tileX, tileY] = newTile;
        }
        // ---------------------------
        public void CreateGoal(int tileX, int tileY, Texture2D tileTexture)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(tileTexture, tilePosition, Tile.CollisionType.GOAL);
            tiles[tileX, tileY] = newTile;
        }
        // ---------------------------
        public void CreateBronzeCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(bronzeCoinTexture, tilePosition, Tile.CollisionType.POINTS, 1);
            tiles[tileX, tileY] = newTile;
        }
        // ---------------------------
        public void CreateSilverCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(silverCoinTexture, tilePosition, Tile.CollisionType.POINTS, 5);
            tiles[tileX, tileY] = newTile;
        }
        // ---------------------------
        public void CreateGoldCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(goldCoinTexture, tilePosition, Tile.CollisionType.POINTS, 10);
            tiles[tileX, tileY] = newTile;
        }
        // ---------------------------
        public Player GetPlayer()
        {
            return player;
        }
        // ---------------------------
        public void Update(GameTime gameTime)
        {
            // Only update the player if the level is not yet complete
            if (complete == false)
            {
                player.Update(gameTime);
            }
        }
        // ---------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < LEVEL_WIDTH; ++x)
            {
                for (int y = 0; y < LEVEL_HEIGHT; ++y)
                {
                    if (tiles[x, y] != null)
                        tiles[x, y].Draw(spriteBatch);
                }
            }
            // Only draw the player if the level is not yet complete
            if (complete == false)
            {
                player.Draw(spriteBatch);
            }
        }
        // ---------------------------
        public void DrawUI(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(mainFont, "SCORE: "+player.GetScore(), 
                new Vector2(10, 10), Color.White);


            if (complete)
            {
                Vector2 stringSize = largeFont.MeasureString("YOU WIN!");
                spriteBatch.DrawString(largeFont, "YOU WIN!", new Vector2(viewSize.X/2 - stringSize.X/2, viewSize.Y/ 2 - stringSize.Y / 2), Color.White);
            }
        }
        // ---------------------------
        public List<Tile> GetTilesInBounds(Rectangle bounds)
        {
            // Create an empty list to fill with tiles
            List<Tile> tilesInBounds = new List<Tile>();

            // Determine the tile coordinate range for tiles in this rect
            int leftTile = (int)Math.Floor((float)bounds.Left / TILE_WIDTH);
            int rightTile = (int)Math.Ceiling(((float)bounds.Right / TILE_WIDTH)) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / TILE_HEIGHT);
            int bottomTile = (int)Math.Ceiling(((float)bounds.Bottom / TILE_HEIGHT)) - 1;

            // Loop through this range and add any tiles to the list.
            for (int y = topTile; y <= bottomTile; ++y)
            {
                for (int x = leftTile; x <= rightTile; ++x)
                {
                    // Only add the tile if it exists (is not null)
                    // And of it is visible
                    Tile newTile = GetTile(x, y);
                    if (newTile != null && newTile.GetVisible() == true)
                        tilesInBounds.Add(newTile);
                }
            }

            // return the populated list
            return tilesInBounds;
        }
        // ---------------------------
        public Tile GetTile(int x, int y)
        {
            if (x < 0 || x >= LEVEL_WIDTH || y < 0 || y >= LEVEL_HEIGHT)
                return null; // Outside bounds 
            else
                return tiles[x, y];
        }
        // ---------------------------
        public void CompleteLevel()
        {
            complete = true;
        }
        // ---------------------------
    }
}
